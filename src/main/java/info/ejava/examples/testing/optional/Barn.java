package info.ejava.examples.testing.optional;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Barn {
    BarnId id;
}
