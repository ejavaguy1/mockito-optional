package info.ejava.examples.testing.optional;

import lombok.Value;

@Value
public class BarnId {
    int id;
}
