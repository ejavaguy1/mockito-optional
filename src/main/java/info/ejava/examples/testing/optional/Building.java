package info.ejava.examples.testing.optional;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Building {
    private int id;
}
