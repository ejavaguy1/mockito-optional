package info.ejava.examples.testing.optional;

public interface BuildingService {
    Wrapper<Building> getBuilding(int id);
}
