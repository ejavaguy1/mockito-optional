package info.ejava.examples.testing.optional;

import lombok.RequiredArgsConstructor;
import java.util.Optional;

@RequiredArgsConstructor
public class BuildingServiceImpl implements BuildingService {
    final FarmService farmService;
    final DecoratorImpl decorator;

    @Override
    public Wrapper<Building> getBuilding(int id) {
        BarnId barnId = new BarnId(id);
        Wrapper<Building> building = farmService.getBarn(barnId)
                .flatMap(this::makeBuilding)
                .filter(this::checkBuilding)
                .map(decorator::decorate)
                .orElseThrow(IllegalArgumentException::new);
        return building;
    }

    protected Optional<Building> makeBuilding(Barn barn) {
        return Optional.ofNullable(barn)
                .map(brn->Building.builder()
                    .id(brn.getId().getId())
                    .build());
    }

    protected boolean checkBuilding(Building building) {
        return Optional.ofNullable(building)
                .filter(bld->bld.getId()!=0)
                .isPresent();
    }
}