package info.ejava.examples.testing.optional;

public class DecoratorImpl {
    public <T> Wrapper<T> decorate(T object) {
        return null==object ? null : new Wrapper(object);
    }
}
