package info.ejava.examples.testing.optional;

import java.util.Optional;

public interface FarmService {
    Optional<Barn> getBarn(BarnId id);
}
