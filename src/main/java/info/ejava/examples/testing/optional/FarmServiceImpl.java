package info.ejava.examples.testing.optional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class FarmServiceImpl implements FarmService {
    private Map<BarnId, Barn> barns = new HashMap();

    public Optional<Barn> getBarn(BarnId barnId) {
        return Optional.ofNullable(barns.get(barnId));
    }
//
//    public Optional<Building> getBuilding(int id) {
//        return Optional.ofNullable(getBarn(new BarnId(id)))
//                .flatMap(b->makeBuilding(b));
//    }
//
//    @Override
//    public Optional<Building> makeBuilding(Optional<Barn> barn) {
//        return barn.map(b->Building.builder().id(b.getId().getId()).build());
//    }
}
