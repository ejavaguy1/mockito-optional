package info.ejava.examples.testing.optional;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Wrapper<T> {
    final T object;
}
