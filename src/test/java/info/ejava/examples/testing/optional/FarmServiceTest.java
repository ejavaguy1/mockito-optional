package info.ejava.examples.testing.optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class FarmServiceTest {
    @InjectMocks
    @Spy
    private BuildingServiceImpl buildingService;
    @Mock
    private FarmService farmService;
    @Spy
    private DecoratorImpl decorator;


    @Test
    void can_mock_optional_response() {
        //given
        Barn myBarn = new Barn(new BarnId(1));
        given(farmService.getBarn(any(BarnId.class))).willReturn(Optional.of(myBarn));
        //when
        Optional<Barn> result = farmService.getBarn(new BarnId(3));
        //then
        then(result).hasValue(myBarn);
    }

    @Test
    void can_mock_flatMap_response() {
        //given
        Barn myBarn = new Barn(new BarnId(1));
        given(farmService.getBarn(any(BarnId.class))).willReturn(Optional.of(myBarn));
        //when
        Wrapper<Building> result = buildingService.getBuilding(3); //whatever ID
        //then
        Wrapper<Building> myBuilding = new Wrapper(new Building(myBarn.getId().getId()));
        then(result).isEqualTo(myBuilding);
    }

    @Test
    void can_mock_flatMap_notPresent() {
        //given
        Barn myBarn = new Barn(new BarnId(1));
        given(farmService.getBarn(any(BarnId.class))).willReturn(Optional.empty());
        //when
        assertThrows(IllegalArgumentException.class,()->
            buildingService.getBuilding(3));
    }

    @Test
    void can_mock_helper_invalid() {
        //given
        Barn myBarn = new Barn(new BarnId(1));
        given(farmService.getBarn(any(BarnId.class))).willReturn(Optional.of(myBarn));
        given(buildingService.checkBuilding(any(Building.class))).willReturn(false);
        //
        assertThrows(IllegalArgumentException.class,()->
                buildingService.getBuilding(3));
    }
}
